/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import { AppRegistry, FlatList, StyleSheet ,Text, View, ActivityIndicator } from 'react-native';
import { ListItem, List, Button,SearchBar } from 'react-native-elements';
//import ajax from './FetchData';

export default class App extends Component {
    // state = {
    //     users: []
    // }
    // async componentDidMount() {
    //     const users = await ajax.fetchUsers();
    //     this.setState({users});
    // }
    //TEST master braches
    constructor(props) {
        super(props);
        this.state ={ isLoading: true}
        // this.state = {
        //     loading: false,
        //     data: [],
        //     page: 1,
        //     seed: 1,
        //     error: null,
        //     refreshing: false,
        // };
    }
    componentDidMount() {
        return fetch('https://facebook.github.io/react-native/movies.json')
        //fetch('https://my-json-server.typicode.com/typicode/demo/comments?fbclid=IwAR2Q7ck-5xOWvzAAqee96zE7hMYfEYUtpS8Pr__LeI6J3Q_wZyqN_2tfaOA')
            .then((response) => response.json())
            .then((responseJson) => {

                this.setState({
                    isLoading: false,
                    dataSource: responseJson.movies,
                }, function(){

                });

            })
            .catch((error) =>{
                console.error(error);
            });
        //this.makeRemoteRequest();
    }
    // makeRemoteRequest = () => {
    //     const { page, seed } = this.state;
    //     //const url = `https://randomuser.me/api/?seed=${seed}&page=${page}&results=20`;
    //     const url = 'http://localhost:3000/users';
    //     this.setState({ loading: true });
    //     fetch(url)
    //         .then(res => res.json())
    //         .then(res => {
    //             this.setState({
    //                 //data: page === 1 ? res.results : [...this.state.data, ...res.results],
    //                 data: page === 1 ? res : [...this.state.data, ...res],
    //                 error: res.error || null,
    //                 loading: false,
    //                 refreshing: false
    //             });
    //         })
    //         .catch(error => {
    //             this.setState({ error, loading: false });
    //         });
    // };
    render() {
        // if(this.state.isLoading){
        //     return(
        //         <View style={{flex: 1, padding: 20}}>
        //             <ActivityIndicator/>
        //         </View>
        //     )
        // }
        return(
            <View style={styles.container} >
                <Text style={styles.h2text}>
                    Black Order
                </Text>
                <FlatList
                    data={this.state.dataSource}
                    //renderItem={({item}) => <Text>{item.title}, {item.releaseYear}</Text>}
                    renderItem={({item}) => <Text>{item.title}, {item.releaseYear}</Text>}
                    keyExtractor={({id}, index) => id}


                    // data={this.state.data}
                    // renderItem={({ item }) => (
                    //     <ListItem
                    //         roundAvatar
                    //         title={`${item.name} ${item.email}`}
                    //         subtitle={item.email}
                    //         //avatar={{ uri: item.picture.thumbnail }}
                    //     />
                    // )}
                    // data={this.state.users}
                    // showsVerticalScrollIndicator={false}
                    // renderItem={({item}) =>
                    //     <View style={styles.flatview}>
                    //         <Text style={styles.name}>{item.name}</Text>
                    //         <Text style={styles.email}>{item.email}</Text>
                    //     </View>
                    // }
                    // keyExtractor={item => item.email}

                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 50,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    h2text: {
        marginTop: 10,
        fontFamily: 'Helvetica',
        fontSize: 36,
        fontWeight: 'bold',
    },
    flatview: {
        justifyContent: 'center',
        paddingTop: 30,
        borderRadius: 2,
    },
    name: {
        fontFamily: 'Verdana',
        fontSize: 18
    },
    email: {
        color: 'red'
    }

});
