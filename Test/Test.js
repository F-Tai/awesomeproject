/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, TextInput, View} from 'react-native';
import { Button } from 'react-native';
import { AppRegistry, Image } from 'react-native';
import { createStackNavigator, StackActions, NavigationActions} from 'react-navigation';

const instructions = Platform.select({
    ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
    android:
        'Double tap R on your keyboard to reload,\n' +
        'Shake or press menu button for dev menu aaassssa',
});

type Props = {};
export default class App extends Component<Props> {
    constructor (props) {
        super(props)
        this.state = {
            input: '',
            label: 'AA'
        }
    }
    handleChangeInput = (text) => {
        this.setState({ input: text })
    }
    handleClick = (text) => {
        this.setState({label : text})
    }

    pic: {
        uri: 'https://upload.wikimedia.org/wikipedia/commons/d/de/Bananavarieties.jpg'
    };
    static navigationOptions = {
        title: 'Welcome',
    };
    render() {
        const { input } = this.state
        //const { navigate } = this.props.navigation;

        return (
            <View style={styles.container}>
                <Text>Home Screen</Text>
                <View style={{flex:1,justifyContent:'center'}}>
                    <View style={{alignItems: 'center'}}>
                        <Greetui name='koa'/>
                        <Text>{this.state.label}</Text>
                    </View>
                </View>

                <View style={styles.mad}>
                    <Text style={{alignItems: 'center'}}>Hàng 1</Text>
                    <Button
                        style={{fontSize: 30, color: 'green'}}
                        onPress={ () => this.ab()}
                        title="Learn More"
                        color="#841584"
                    />
                    <Button
                        title="Go to Jane's profile"
                        onPress={() => this.props.navigation.navigate('DetailsScreen')
                        }
                    />
                    <TextInput style={{height: 40, borderColor: 'gray', borderWidth: 1,backgroundColor: 'white'}}
                               onChangeText={this.handleChangeInput}
                               value={input}></TextInput>
                    {/*<Image source={this.pic} style={{width: 193, height: 110}}/>*/}
                    <More hoten={"Tai"} press={(text) => this.handleClick(text)}></More>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <Text>Home Screen</Text>
                    </View>
                </View>
                <View style={{flex:1,justifyContent:'center'}}>
                    <View style={{alignItems: 'center'}}>
                        <Greeting name='Valeerassss' />
                    </View>
                    <View style={{alignItems: 'center'}}>
                        <Greetui name='Kae'/>
                    </View>
                </View>
                <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
                    <View style={{width: 50, height: 50, backgroundColor: 'powderblue'}} />
                    <View style={{width: 100, height: 100, backgroundColor: 'skyblue'}} />
                    <View style={{width: 150, height: 150, backgroundColor: 'steelblue'}} />
                </View>

            </View>
        );
    }
    ab(){
        alert('1')
    }
}
class More extends  React.Component{

    render(){
        return (
            <View style={{width:100,height:100,backgroundColor: 'red'}}>
                <Button
                    style={{fontSize: 30, color: 'skyblue',width:10,height:10}}
                    onPress={ () => this.props.press('SSSS')}
                    title= {this.props.hoten}
                    color="#841584"
                />
            </View>
        );
    }
}

class HomeScreen extends React.Component {
    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Text>Home Screen</Text>
                <Button
                    title="Go to Details"
                    onPress={() => {
                        this.props.navigation.dispatch(StackActions.reset({
                            index: 0,
                            actions: [
                                NavigationActions.navigate({ routeName: 'Details' })
                            ],
                        }))
                    }}
                />
            </View>
        );
    }
}

class DetailsScreen extends React.Component {
    render() {
        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Text>Details Screen</Text>
            </View>
        );
    }
}

class Greeting extends Component {
    render() {
        return (
            <View style={{alignItems: 'center'}}>
                <Text>Hello {this.props.name}!</Text>
            </View>
        );
    }
}
class Greetui extends Component {
    render() {
        return (
            <View style={{alignItems: 'center'}}>
                <Text>Hello {this.props.name}!</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    mad: {
        flex:7,
        justifyContent:'flex-start',
        alignItems:"center",
        width: "90%",
        marginBottom: 10,
        borderRadius: 50,
        backgroundColor:'gray',
        elevation:10
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        //backgroundColor: 'blue',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
});
