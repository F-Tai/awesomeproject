/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import { AppRegistry, FlatList, StyleSheet ,Text, View, ActivityIndicator } from 'react-native';
import { ListItem, List, Button,SearchBar } from 'react-native-elements';

export default class App extends React.PureComponent {
    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            data: [],
            page: 1,
            seed: 1,
            error: null,
            refreshing: false,
        };
    }

    componentDidMount() {
        this.makeRemoteRequest();
    }

    makeRemoteRequest = () => {
        const { page, seed } = this.state;
        const url = `https://randomuser.me/api/?seed=${seed}&page=${page}&results=20`;
        this.setState({ loading: true });
        fetch(url)
            .then(res => res.json())
            .then(res => {
                this.setState({
                    data: page === 1 ? res.results : [...this.state.data, ...res.results],
                    error: res.error || null,
                    loading: false,
                    refreshing: false
                });
            })
            .catch(error => {
                this.setState({ error, loading: false });
            });
    };
    renderHeader = () => {
        return <SearchBar placeholder="Type Here..." lightTheme round />;
    };
    renderFooter = () => {
        if (!this.state.loading) return null;

        return (
            <View
                style={{
                    paddingVertical: 20,
                    borderTopWidth: 1,
                    borderColor: "#CED0CE"
                }}
            >
                <ActivityIndicator animating size="large" />
            </View>
        );
    };

    render() {
        return(
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
                <View style={{flex:9, width: 400}}>
                    <List containerStyle={{ borderTopWidth: 0, borderBottomWidth: 0 }}>
                        <FlatList
                            data={this.state.data}
                            renderItem={({ item }) => (
                                <ListItem
                                    roundAvatar
                                    title={`${item.name.first} ${item.name.last}`}
                                    subtitle={item.email}
                                    avatar={{ uri: item.picture.thumbnail }}
                                />
                            )}
                            // keyExtractor={item => item.email}
                            // containerStyle={{ borderBottomWidth: 0 }}
                            // ItemSeparatorComponent={this.renderSeparator}
                            // ListHeaderComponent={this.renderHeader}
                            // ListFooterComponent={this.renderFooter}
                        />
                    </List>
                </View>
                <View style={{flex:1}}>
                    <Text>AA</Text>
                    <Button title='Hello World!'  />
                </View>
            </View>
        );
    }
}
