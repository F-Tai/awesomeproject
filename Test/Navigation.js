/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import { Platform, StyleSheet, Text, View, Button, TextInput,FlatList} from 'react-native';
import { createStackNavigator } from 'react-navigation';

// import { StackNavigator } from 'react-navigation';
// import Settings from './screens/Settings';
// import Home from './screens/Home';
//
// type Props = {};
// export default class App extends Component<Props> {
//       render() {
//           return (
//               <AppNavigator/>
//           );
//     }
// };
// const AppNavigator = StackNavigator({
//     SettingScreen: { screen: Settings },
//     HomeScreen: { screen: Home }
// });
////////
//
// export default createStackNavigator({
//     Home: {
//         screen: HomeScreen
//     },
// });
//
class HomeScreen extends React.Component {
    static navigationOptions = {
        title: 'Home',
    };
    constructor(props) {
        super(props);
        this.state = { text: 'Useless Placeholder' };
    }
    handleChangeInput = (text) => {
        this.setState({ input: text })
    }
    render() {
        const { input } = this.state
        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Text>Home Screen</Text>
                <TextInput style={{height: 40,width: 200, borderColor: 'gray', borderWidth: 1,backgroundColor: 'white'}}
                           onChangeText={this.handleChangeInput}
                           placeholder='INPUT WITH ERROR MESSAGE'
                           value={input}></TextInput>
                <Button
                    title="Go to Details"
                    /* 1. Navigate to the Details route with params */
                    onPress={() => this.props.navigation.navigate('Details',{
                        itemId: 86,
                        otherParam: 'Hello ' + input + ' !!!!',
                    })}
                />
            </View>
        );
    }
}
class DetailsScreen extends React.Component {
    // static navigationOptions = {
    //     title: 'Details',
    // };
    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.getParam('otherParam', 'A Nested Details Screen'),
        };
    };
    render() {
        /* 2. Get the param, provide a fallback value if not available */
        const { navigation } = this.props;
        const itemId = navigation.getParam('itemId', 'NO-ID');
        const otherParam = navigation.getParam('otherParam', 'some default value');

        return (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Text>Details Screen</Text>
                <Text>itemId: {JSON.stringify(itemId)}</Text>
                <Text>otherParam: {JSON.stringify(otherParam)}</Text>

                <Button
                    title="Go to Details... again"
                    onPress={() => this.props.navigation.push('Details', {
                        itemId: Math.floor(Math.random() * 100),
                    })}
                />
                <Button
                    title="Go to Home"
                    onPress={() => this.props.navigation.navigate('Home')}
                />
                <Button
                    title="Go back"
                    onPress={() => this.props.navigation.goBack()}
                />
                <Button
                    title="Update the title"
                    onPress={() => this.props.navigation.setParams({otherParam: 'Updated!'})}
                />

                <FlatList
                    data={[{key: 'a', cont: 'aaaa'}, {key: 'b', cont: 'loop'}]}
                    renderItem={({item}) =>
                        <View style={{height: 100, width: 200, alignItems: 'center', justifyContent:'center',
                            borderColor: 'white', backgroundColor: 'gray'}}>
                            <Text>{item.key} + {item.cont}
                            </Text>
                        </View>
                    }
                />

            </View>
        );
    }
}
const RootStack = createStackNavigator(
    {
        Home: HomeScreen,
        Details: DetailsScreen,
    },
    {
        initialRouteName: 'Home',
    }
);
export default class App extends React.Component {
    render() {
        return <RootStack />;
    }
}
